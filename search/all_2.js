var searchData=
[
  ['ball_2dbalancing_20platform_20equation_20derivation_0',['Ball-Balancing Platform Equation Derivation',['../_h_w0x02.html',1,'']]],
  ['ball_2dbalancing_20platform_20simulation_1',['Ball-Balancing Platform Simulation',['../_h_w0x03.html',1,'']]],
  ['betamat_2',['betamat',['../_t_p_main_8py.html#abe7a6100367385635ae3f26766d36861',1,'TPMain']]],
  ['bno055_3',['BNO055',['../class_b_n_o055_1_1_b_n_o055.html',1,'BNO055.BNO055'],['../class_b_n_o055_t_p_1_1_b_n_o055.html',1,'BNO055TP.BNO055']]],
  ['bno055_2epy_4',['BNO055.py',['../_b_n_o055_8py.html',1,'']]],
  ['bno055tp_2epy_5',['BNO055TP.py',['../_b_n_o055_t_p_8py.html',1,'']]],
  ['brt_6',['brt',['../_lab__0x01_8py.html#aec7b6973ccc11a82977c2a391135ce92',1,'Lab_0x01']]],
  ['buttonint_7',['ButtonInt',['../class_d_r_v8847_1_1_d_r_v8847.html#a167391c3337294abfdc4cac65938ce9a',1,'DRV8847.DRV8847.ButtonInt()'],['../_lab__0x01_8py.html#a87006ae4b27231f7f74a3813aed69285',1,'Lab_0x01.ButtonInt()']]],
  ['buttonpressed_8',['buttonPressed',['../_lab__0x01_8py.html#ad2c3bde3184d7a9923478e22e9276a96',1,'Lab_0x01']]]
];
