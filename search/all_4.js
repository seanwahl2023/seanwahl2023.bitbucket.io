var searchData=
[
  ['dc1_0',['DC1',['../_lab3_main_8py.html#add9043a989824e34ab03373a258ae8ed',1,'Lab3Main.DC1()'],['../_lab4_main_8py.html#ada3d262151e3cefb1459203899aa6ada',1,'Lab4Main.DC1()'],['../_lab5_main_8py.html#a36d58033dea2f21bf5c5b5ce1f993cdd',1,'Lab5Main.DC1()']]],
  ['dc2_1',['DC2',['../_lab3_main_8py.html#acd1bd403ed92bb2442559ac0c2e98e1c',1,'Lab3Main.DC2()'],['../_lab4_main_8py.html#a6af57021bc4ec178ef098eb3d7eb5274',1,'Lab4Main.DC2()'],['../_lab5_main_8py.html#ac3c964523ceb9f8aac2d52e911edf6be',1,'Lab5Main.DC2()']]],
  ['delta_2',['delta',['../classencoder_1_1_encoder.html#ad017c0a5f382fe0dac6ed8920ce90635',1,'encoder.Encoder.delta()'],['../classencoder2_1_1_encoder.html#a0fbe8fb41d7088c594ccded575b0ce77',1,'encoder2.Encoder.delta()'],['../_lab2_main_8py.html#a2e07a04cdd403b2d27b37074632042b9',1,'Lab2Main.delta()'],['../_lab3_main_8py.html#a4089a998d8bfb11bce0ea8c676633a84',1,'Lab3Main.delta()'],['../_lab4_main_8py.html#a20e65b43619410639475ebf02e638cf2',1,'Lab4Main.delta()']]],
  ['disable_3',['disable',['../class_d_r_v8847_1_1_d_r_v8847.html#acd9dbef9212b3014eab18a57a6e0f13a',1,'DRV8847.DRV8847.disable(self)'],['../class_d_r_v8847_1_1_d_r_v8847.html#acd9dbef9212b3014eab18a57a6e0f13a',1,'DRV8847.DRV8847.disable(self)']]],
  ['doneflag_4',['doneFlag',['../_t_p_main_8py.html#a028d5d8bb077f78daf7fae20833487ec',1,'TPMain']]],
  ['drv8847_5',['DRV8847',['../class_d_r_v8847_1_1_d_r_v8847.html',1,'DRV8847']]],
  ['drv8847_2epy_6',['DRV8847.py',['../_lab_013_2_d_r_v8847_8py.html',1,'(Global Namespace)'],['../_lab_014_2_d_r_v8847_8py.html',1,'(Global Namespace)']]]
];
