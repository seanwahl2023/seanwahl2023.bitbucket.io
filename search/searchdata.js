var indexSectionsWithContent =
{
  0: "_abcdefgiklmnopqrstuvwxyz",
  1: "bcdempqs",
  2: "t",
  3: "bcdelmpst",
  4: "_acdefgmnoprstuwxyz",
  5: "abcdefiklmnpstvwxyz",
  6: "blst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Pages"
};

