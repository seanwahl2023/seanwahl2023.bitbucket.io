var searchData=
[
  ['sawwave_0',['SawWave',['../_lab__0x01_8py.html#a5f5e48f2ccc6c8e5fee2ad0b3e244eaa',1,'Lab_0x01']]],
  ['sean_27s_20portfolio_1',['Sean&apos;s Portfolio',['../index.html',1,'']]],
  ['set_5fduty_2',['set_duty',['../classmotor_1_1_motor.html#a08f41a32c8b122a8b94496fef2a0a901',1,'motor.Motor.set_duty()'],['../classmotor2_1_1_motor.html#a3df91dc4baa33bdb319b093328355495',1,'motor2.Motor.set_duty()'],['../classmotor3_1_1_motor.html#a173795d52b17e03c92f67b37811ea842',1,'motor3.Motor.set_duty()'],['../classmotor_t_p_1_1_motor.html#ab43a70704c1480f5b055e71ed15a424d',1,'motorTP.Motor.set_duty()']]],
  ['set_5fgain_3',['set_gain',['../classclosedloop_1_1_closed_loop.html#ad34994d4afc42c53512773585168669b',1,'closedloop.ClosedLoop.set_gain()'],['../classclosedloop2_1_1_closed_loop.html#a7acd5cdb8abf65dbcdde8e036d81b3f8',1,'closedloop2.ClosedLoop.set_gain()'],['../classclosedloop_t_p_1_1_closed_loop.html#a9fd7ca41c15fe7e6a5f9ebdf3a70f740',1,'closedloopTP.ClosedLoop.set_gain()']]],
  ['share_4',['Share',['../classshares_1_1_share.html',1,'shares']]],
  ['shares_2epy_5',['shares.py',['../_lab_012_2shares_8py.html',1,'(Global Namespace)'],['../_lab_013_2shares_8py.html',1,'(Global Namespace)'],['../_lab_014_2shares_8py.html',1,'(Global Namespace)'],['../_lab_015_2shares_8py.html',1,'(Global Namespace)'],['../_term_01_project_2shares_8py.html',1,'(Global Namespace)']]],
  ['sinewave_6',['SineWave',['../_lab__0x01_8py.html#afeb30c54a0ede1c859e111c21fa5df31',1,'Lab_0x01']]],
  ['squarewave_7',['SquareWave',['../_lab__0x01_8py.html#aa67eb9ca2dfeaa52e6ce2f7a38df3f56',1,'Lab_0x01']]],
  ['state_8',['state',['../_lab__0x01_8py.html#a09bab872442d4559f2857c92d47ac1f9',1,'Lab_0x01']]]
];
